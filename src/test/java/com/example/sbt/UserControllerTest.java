package com.example.sbt;


import com.example.sbt.controllers.ErrorController;
import com.example.sbt.controllers.UserController;
import com.example.sbt.users.UserService;
import jakarta.servlet.Filter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.restdocs.RestDocsAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.setup.ConfigurableMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.setup.MockMvcConfigurer;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {UserController.class, ErrorController.class})
@AutoConfigureRestDocs
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    UserService userService;


    @Test
    void successRegister() throws Exception {
        ResultActions result = mockMvc.perform(
                post("/users/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {"login":"vasia","password":"0000" }
                                """)
        ).andDo(print());

        result
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"));

        result.andDo(document("{class-name}/{method-name}",
                requestFields(
                        fieldWithPath("login").description("login of user which register"),
                        fieldWithPath("password").description("password of user which register")
                ),
                responseFields(
                        fieldWithPath("status").description("operation status (OK|FAIL)"),
                        fieldWithPath("error").description("Error text if fail status").optional().type(String.class)
                )
        ));

    }

    @Test
    void invalidRequestRegister() throws Exception {
        ResultActions result = mockMvc.perform(
                post("/users/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {"login":"vasia" }
                                """)
        ).andDo(print());

        result
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.error").value("login and password is required"));

        result.andDo(document("{class-name}/{method-name}"));

    }


}
