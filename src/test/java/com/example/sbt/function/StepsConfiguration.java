package com.example.sbt.function;

import com.example.sbt.function.steps.KafkaSteps;
import com.example.sbt.function.steps.UserSteps;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.kafka.test.EmbeddedKafkaBroker;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@TestConfiguration
class StepsConfiguration {

    public static final LocalDateTime NOW = LocalDateTime.now();

    @Bean
    UserSteps userSteps(TestRestTemplate testRestTemplate) {
        return new UserSteps(testRestTemplate);
    }

    @Bean
    KafkaSteps kafkaSteps(EmbeddedKafkaBroker embeddedKafkaBroker) {
        return new KafkaSteps(embeddedKafkaBroker);
    }

}
