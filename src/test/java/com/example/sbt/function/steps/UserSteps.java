package com.example.sbt.function.steps;

import com.example.sbt.dto.UserRegisterRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.Map;

@RequiredArgsConstructor
public class UserSteps {

    private final TestRestTemplate testRestTemplate;

    public ResponseEntity<Map<String, String>> sendUserRegisterRequest(UserRegisterRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        HttpEntity<?> requestEntity = new HttpEntity<>(request, headers);
        ResponseEntity<Map<String, String>> response = testRestTemplate.exchange(
                "/users/register",
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<Map<String, String>>() {
                }
        );
        return response;
    }
}
