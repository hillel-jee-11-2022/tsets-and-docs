package com.example.sbt.function.steps;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.utils.KafkaTestUtils;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class KafkaSteps {
    private final EmbeddedKafkaBroker embeddedKafkaBroker;

    public Consumer<String, String> consumerCreate(List<String> topics) {
        Map<String, Object> props = KafkaTestUtils.consumerProps("testGroupId", "false", embeddedKafkaBroker);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        Consumer<String, String> consumer = new DefaultKafkaConsumerFactory<String, String>(props)
                .createConsumer();
        consumer.subscribe(topics);
        return consumer;
    }
}
