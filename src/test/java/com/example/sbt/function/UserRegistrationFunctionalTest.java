package com.example.sbt.function;

import com.example.sbt.SbtApplication;
import com.example.sbt.dto.UserRegisterRequest;
import com.example.sbt.function.steps.KafkaSteps;
import com.example.sbt.function.steps.UserSteps;
import com.example.sbt.persistance.complex.ContainerConfig;
import com.example.sbt.users.UserRegisterEvent;
import com.example.sbt.users.UserRepository;
import com.example.sbt.users.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;

import java.time.*;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {ContainerConfig.class,SbtApplication.class, StepsConfiguration.class}
)
@EmbeddedKafka(partitions = 1, brokerProperties = {"listeners=PLAINTEXT://localhost:9092", "port=9092"})
public class UserRegistrationFunctionalTest {

    public static final String LOGIN = "VASIA";
    public static final String PASSWORD = "0000";
    public static final String TEST_TOPIC = "test-topic";

    @Autowired
    UserSteps userSteps;
    @Autowired
    KafkaSteps kafkaSteps;
    @Autowired
    UserRepository userRepository;
    @MockBean
    Clock clock;
    @Autowired
    ObjectMapper objectMapper;

    @BeforeEach
    void setUp(){
        when(clock.instant()).thenReturn(Instant.now());
        when(clock.getZone()).thenReturn(ZoneId.systemDefault());
    }


    @Test
    public void shouldUserRegister() throws Exception {
        Consumer<String, String> consumer = kafkaSteps.consumerCreate(List.of(TEST_TOPIC));

        UserRegisterRequest request = new UserRegisterRequest();
        request.setLogin(LOGIN);
        request.setPassword(PASSWORD);

        ResponseEntity<Map<String, String>> response = userSteps.sendUserRegisterRequest(request);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(userRepository.findByLogin(LOGIN)).hasValue(new User(LOGIN, PASSWORD));

        assertReceiveUserInKafka(consumer, LOGIN);
    }

    private void assertReceiveUserInKafka(Consumer<String, String> consumer, String login) throws Exception {
        ConsumerRecord<String, String> record = KafkaTestUtils.getSingleRecord(consumer, TEST_TOPIC, Duration.ofSeconds(10));
        assertThat(record.value()).isEqualTo(
                objectMapper.writeValueAsString(UserRegisterEvent.builder()
                        .login(login)
                        .registerDate(LocalDateTime.now(clock))
                        .build()
                )
        );

    }

}
