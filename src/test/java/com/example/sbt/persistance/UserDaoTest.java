package com.example.sbt.persistance;

import com.example.sbt.persistance.dao.UserDao;
import com.example.sbt.users.model.User;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@Testcontainers
public class UserDaoTest {

    @Container
    private PostgreSQLContainer postgresContainer = new PostgreSQLContainer("postgres:11.1")
            .withDatabaseName("tests-db")
            .withUsername("sa")
            .withPassword("sa");

    private UserDao userDao;

    @BeforeEach
    void setUp(){
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(postgresContainer.getDriverClassName());
        config.setJdbcUrl(postgresContainer.getJdbcUrl());
        config.setUsername(postgresContainer.getUsername());
        config.setPassword(postgresContainer.getPassword());

        DataSource datasource = new HikariDataSource(config);

        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(datasource);

        initDb(jdbcTemplate);

        userDao = new UserDao(jdbcTemplate);
    }

    @Test
    void exampleTest(){
        assertThat(userDao.findByLogin("vasia")).isEmpty();
        User user = new User("vasia", "0000");
        userDao.save(user);
        assertThat(userDao.findByLogin("vasia")).hasValue(user);
    }

    @Test
    void exampleTest2(){
        Optional<User> result1 = userDao.findByLogin("vasia");
        assertThat(result1).isEmpty();
    }



    private void initDb(NamedParameterJdbcTemplate jdbcTemplate) {
        jdbcTemplate.execute("""
                CREATE TABLE users(
                    login VARCHAR(255) NOT NULL PRIMARY KEY , 
                    password VARCHAR(255)
                )
                """,
                PreparedStatement::execute
        );
    }




}
