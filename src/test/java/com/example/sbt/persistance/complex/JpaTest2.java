package com.example.sbt.persistance.complex;

import com.example.sbt.SbtApplication;
import com.example.sbt.persistance.jpa.JpaUserRepository;
import com.example.sbt.persistance.jpa.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {SbtApplication.class,ContainerConfig.class})
public class JpaTest2 {

    @Autowired
    private JpaUserRepository jpaUserRepository;

//    @DynamicPropertySource
//    static void datasourceProps(final DynamicPropertyRegistry registry) {
//        registry.add("spring.jpa.hibernate.ddl-auto",()->"create-drop");
//    }

    @Test
    void test(){
        UserEntity user = new UserEntity("vasia", "1111");
        jpaUserRepository.save(user);

        assertThat(jpaUserRepository.findById("vasia")).hasValue(user);
    }

}
