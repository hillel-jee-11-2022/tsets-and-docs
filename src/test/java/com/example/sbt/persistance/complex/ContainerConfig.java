package com.example.sbt.persistance.complex;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.sql.DataSource;
import java.sql.PreparedStatement;

@TestConfiguration
public class ContainerConfig {
    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Bean(destroyMethod = "stop", initMethod = "start")
    static PostgreSQLContainer postgresContainer() {
        return new PostgreSQLContainer("postgres:11.1")
                .withDatabaseName("integration-tests-db")
                .withUsername("sa")
                .withPassword("sa");
    }

    @Bean
    static DataSource dataSource(PostgreSQLContainer postgresContainer) {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(postgresContainer.getDriverClassName());
        config.setJdbcUrl(postgresContainer.getJdbcUrl());
        config.setUsername(postgresContainer.getUsername());
        config.setPassword(postgresContainer.getPassword());

        return new HikariDataSource(config);
    }

    @PostConstruct
    void initDb() {
        jdbcTemplate.execute("""
                CREATE TABLE IF NOT EXISTS users(
                    login VARCHAR(255) NOT NULL PRIMARY KEY , 
                    password VARCHAR(255)
                )
                """,
                PreparedStatement::execute
        );
    }
}
