package com.example.sbt.persistance.complex;

import com.example.sbt.SbtApplication;
import com.example.sbt.persistance.dao.UserDao;
import com.example.sbt.users.model.User;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = {SbtApplication.class, ContainerConfig.class})
public class UserDaoTest2 {

    @Autowired
    private UserDao userDao;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Test
    void exampleTest(){
        assertThat(userDao.findByLogin("vasia")).isEmpty();
        User user = new User("vasia", "0000");
        userDao.save(user);
        assertThat(userDao.findByLogin("vasia")).hasValue(user);
    }

    @Test
    void exampleTest2(){//ATENTION!!!!
        assertThat(userDao.findByLogin("vasia")).isEmpty();
        User user = new User("vasia", "0000");
        userDao.save(user);
        assertThat(userDao.findByLogin("vasia")).hasValue(user);
    }

    @AfterEach
    void clear(){
        namedParameterJdbcTemplate.update("TRUNCATE users",new MapSqlParameterSource());
    }

}
