package com.example.sbt.persistance;

import com.example.sbt.SbtApplication;
import com.example.sbt.persistance.jpa.JpaUserRepository;
import com.example.sbt.persistance.jpa.UserEntity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = SbtApplication.class)
@Testcontainers
public class JpaTest {

    @Container
    private static PostgreSQLContainer postgresContainer = new PostgreSQLContainer("postgres:11.1")
            .withDatabaseName("tests-db")
            .withUsername("sa")
            .withPassword("sa");

    @Autowired
    private JpaUserRepository jpaUserRepository;


    @DynamicPropertySource
    static void datasourceProps(final DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);
        registry.add("spring.datasource.driver-class-name", postgresContainer::getDriverClassName);
        registry.add("spring.jpa.hibernate.ddl-auto",()->"create-drop");
    }

    @AfterEach
    void init(){
        jpaUserRepository.deleteAll();
    }

    @Test
    void test(){

        assertThat(jpaUserRepository.findById("vasia")).isEmpty();
        UserEntity user = new UserEntity("vasia", "1111");
        jpaUserRepository.save(user);
        assertThat(jpaUserRepository.findById("vasia")).hasValue(user);
    }

    @Test
    void test2(){
        assertThat(jpaUserRepository.findById("vasia")).isEmpty();
        UserEntity user = new UserEntity("vasia", "1112");
        jpaUserRepository.save(user);
        assertThat(jpaUserRepository.findById("vasia")).hasValue(user);
    }

}
