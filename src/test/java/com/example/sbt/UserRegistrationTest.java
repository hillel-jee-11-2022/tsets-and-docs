package com.example.sbt;


import com.example.sbt.users.UserRegisterEvent;
import com.example.sbt.users.UserRepository;
import com.example.sbt.users.UserService;
import com.example.sbt.users.UsersConfiguration;
import com.example.sbt.users.model.User;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.core.KafkaTemplate;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = {UsersConfiguration.class})
public class UserRegistrationTest {

    public static final String LOGIN = "vasia";
    public static final String PASSWORD = "0000";
    public static final User USER = new User(LOGIN, PASSWORD);
    public static final String TOPIC = "test-topic";
    @MockBean
    KafkaTemplate<String, UserRegisterEvent> kafkaTemplate;

    @MockBean
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Captor
    ArgumentCaptor<UserRegisterEvent> userRegisterEventArgumentCaptor;
    @MockBean
    Clock clock;

    @Test
    void shoulRegisterUser() {
        when(userRepository.save(USER)).thenReturn(USER);
        when(userRepository.findByLogin(LOGIN)).thenReturn(Optional.empty());
        when(clock.getZone()).thenReturn(ZoneId.systemDefault());
        when(clock.instant()).thenReturn(LocalDateTime.now().toInstant(ZoneOffset.UTC));

        userService.register(LOGIN, PASSWORD);

        verify(userRepository).save(USER);
        verify(kafkaTemplate).send(eq(TOPIC), userRegisterEventArgumentCaptor.capture());

        UserRegisterEvent sendedEvent = userRegisterEventArgumentCaptor.getValue();
        assertThat(sendedEvent.getLogin()).isEqualTo(LOGIN);
        assertThat(sendedEvent.getRegisterDate()).isNotNull();
    }

    @Test
    void shoulNotRegisterUserWhenUserExists() {
        when(userRepository.findByLogin(LOGIN)).thenReturn(Optional.of(USER));

        assertThatThrownBy(() -> userService.register(LOGIN, PASSWORD))
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessage("User already exists");

        verify(userRepository,never()).save(any());
        verify(kafkaTemplate,never()).send(eq(TOPIC), any());
    }
}
