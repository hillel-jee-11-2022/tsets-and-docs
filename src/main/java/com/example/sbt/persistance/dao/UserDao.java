package com.example.sbt.persistance.dao;

import com.example.sbt.users.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@RequiredArgsConstructor
@Repository
public class UserDao {
    public static final String INSERT = "INSERT INTO users(login,password) VALUES (:login,:pass)";
    public static final String SELECT_BY_LOGIN = "SELECT login, password FROM users WHERE login=:login";
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public Optional<User> findByLogin(String login){
        return jdbcTemplate.query(
                SELECT_BY_LOGIN,
                new MapSqlParameterSource("login", login),
                (rs,i)->new User(rs.getString("login"), rs.getString("password"))
        ).stream().findFirst();
    }

    public User save(User user){
        jdbcTemplate.update(
                INSERT,
                new BeanPropertySqlParameterSource(user)
        );
        return user;
    }


}
