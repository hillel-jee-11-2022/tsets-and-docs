package com.example.sbt.users;


import com.example.sbt.users.model.User;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class UserRepository {

    Map<String, User> users = new HashMap<>();


    public Optional<User> findByLogin(String login){
        return Optional.of(login).map(users::get);
    }

    public User save(User user){
        users.put(user.getLogin(),user);
        return user;
    }

}
