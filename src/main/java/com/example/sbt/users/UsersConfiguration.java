package com.example.sbt.users;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

import java.time.Clock;

@Configuration
public class UsersConfiguration {
    @Bean
    UserRepository userRepository() {
        return new UserRepository();
    }

    @Bean
    UserEventSender userEventSender(
            @Value("${kafka.topics.userRegister.name}") String topicName,
            KafkaTemplate<String,UserRegisterEvent> kafkaTemplate,
            Clock clock
    ){
        return new UserEventSender(
                topicName,
                clock,
                kafkaTemplate
        );
    }

    @Bean
    UserService userService(UserRepository userRepository, UserEventSender userEventSender){
        return new UserService(userRepository,userEventSender);
    }
}
