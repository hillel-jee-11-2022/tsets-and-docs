package com.example.sbt.users;

import com.example.sbt.users.model.User;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserEventSender userEventSender;

    public void register(String login, String password) {
        if (userRepository.findByLogin(login).isPresent()) {
            throw new IllegalArgumentException("User already exists");
        }

        User user = new User(login, password);
        user = userRepository.save(user);
        userEventSender.send(user.getLogin());
    }

}
