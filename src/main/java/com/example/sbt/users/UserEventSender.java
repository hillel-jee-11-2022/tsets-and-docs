package com.example.sbt.users;

import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;

import java.time.Clock;
import java.time.LocalDateTime;

@RequiredArgsConstructor
public class UserEventSender {
    private final String topicName;
    private final Clock clock;
    private final KafkaTemplate<String, UserRegisterEvent> kafkaTemplate;

    public void send(String login) {
        kafkaTemplate.send(topicName,
                UserRegisterEvent.builder()
                        .login(login)
                        .registerDate(LocalDateTime.now(clock))
                        .build()
        );
    }
}
