package com.example.sbt.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Value;

@Value
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {
    Status status;
    String error;
    Object data;


    public static Response ok() {
        return new Response(Status.OK, null, null);
    }

    public static Response ok(Object data) {
        return new Response(Status.OK, null, data);
    }

    public static Response fail(String message) {
        return new Response(Status.FAIL, message, null);
    }

}
