package com.example.sbt.dto;

import lombok.Data;

@Data
public class UserRegisterRequest {
    private String login;
    private String password;
}
