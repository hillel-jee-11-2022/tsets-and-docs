package com.example.sbt.controllers;

import com.example.sbt.contracts.api.Example1Api;
import com.example.sbt.contracts.model.Request01;
import com.example.sbt.contracts.model.Response01;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExampleController implements Example1Api {
    @Override
    public ResponseEntity<Response01> exampleGet(String name) {
        Response01 body = new Response01("vasia");
        return ResponseEntity.ok(body);
    }

    @Override
    public ResponseEntity<Response01> examplePost(Request01 request01, String name) {
        Response01 body = Response01.builder()
                .name("vasia")
                .build();
        return ResponseEntity.ok(body);
    }
}
