package com.example.sbt.controllers;

import com.example.sbt.dto.Response;
import com.example.sbt.dto.UserRegisterRequest;
import com.example.sbt.users.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("register")
    Response register(@RequestBody UserRegisterRequest registerRequest){
        if (registerRequest.getLogin() == null || registerRequest.getPassword() == null){
            throw new IllegalArgumentException("login and password is required");
        }

        userService.register(registerRequest.getLogin(),registerRequest.getPassword());
        return Response.ok();
    }
}
